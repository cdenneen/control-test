class profile::linux::firewall::pre {
  Firewall {
    require => undef,
  }

# Disable due to selective purges of firewallchain
#    resources { "firewall":
#      purge => true
#    }

  firewallchain { 'LOGGING:filter:IPv4':
    ensure => present,
  }
  # Avoid removing Docker rules:
  firewallchain { 'FORWARD:filter:IPv4':
    purge  => true,
    ignore => [ 'docker', 'DOCKER-ISOLATION' ],
  }
  firewallchain { 'DOCKER:filter:IPv4':
    purge => false,
  }
  firewallchain { 'DOCKER:nat:IPv4':
    purge => false,
  }
  firewallchain {'DOCKER-ISOLATION:filter:IPv4':
    purge => false,
  }
  firewallchain { 'POSTROUTING:nat:IPv4':
    purge  => true,
    ignore => [ 'docker', '172.17' ],
  }
  firewallchain { 'PREROUTING:nat:IPv4':
    purge  => true,
    ignore => [ 'DOCKER' ],
  }

  #ensure input rules are cleaned out
  firewallchain { 'INPUT:filter:IPv4':
    ensure => present,
    purge  => true,
  }
  # Default firewall rules
  firewall { '000 accept all icmp':
    proto  => 'icmp',
    action => 'accept',
  }->
  firewall { '001 accept all to lo interface':
    proto   => 'all',
    iniface => 'lo',
    action  => 'accept',
  }->
  firewall { '002 accept related established rules':
    proto  => 'all',
    state  => ['RELATED', 'ESTABLISHED'],
    action => 'accept',
  }->
  firewall { '003 allow ssh access':
    dport  => '22',
    proto  => tcp,
    action => accept,
    state  => ['NEW'],
  }
}
