class profile::linux {

  # Firewall
  require ::firewall
  contain ::profile::linux::firewall::pre
  contain ::profile::linux::firewall::post

  $firewall_rules = lookup('firewall', { 'default_value' => {} })
  $firewall_rules.each |$rule,$params| {
    firewall { $rule:
      *       => $params,
      before  => Class['profile::linux::firewall::post'],
      require => Class['profile::linux::firewall::pre'],
    }
  }
  Class['::firewall'] ~> Class['profile::linux']
}
