class profile::example {
  notice('This is example profile')
  notify{'Example Profile':}
  if $facts[kernel] == 'Linux' {
    include profile::linux
  }
}
