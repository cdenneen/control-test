require 'puppet'
require 'beaker-rspec/spec_helper'
require 'beaker-rspec/helpers/serverspec'
require 'beaker/puppet_install_helper'
require 'beaker/module_install_helper'
require 'puppetlabs_spec_helper/rake_tasks'
require 'winrm'

Rake::Task['spec_prep'].invoke
at_exit { Rake::Task['spec_clean'].invoke }

run_puppet_install_helper unless ENV['BEAKER_provision'] == 'no'

# put local configuration and setup into spec_helper_acceptance_local
begin
  require 'spec_helper_acceptance_local' if File.exists?(File.join(File.dirname(__FILE__),'spec_helper_acceptance_local.rb'))
rescue LoadError => e
  handle_error(e)
end

RSpec.configure do |c|
  proj_root = File.expand_path(File.join(File.dirname(__FILE__), '..'))
  proj_name = proj_root.split('/').last
  # Readable test descriptions
  c.formatter = :documentation

  # Configure all nodes in nodeset
  c.before :suite do
    hosts.each do |host|
      host['hieradatadir'] = "/tmp/hieradata"
      hierarchy = [
        'role/%{role}/%{ap_env}/%{ap_region}',
        'role/%{role}/%{ap_env}/%{datacenter}/%{securityzone}',
        'role/%{role}/%{ap_env}/%{datacenter}',
        'role/%{role}/%{ap_env}',
        'role/%{role}',
        'region/%{ap_region}',
        'dc/%{datacenter}/%{securityzone}',
        'dc/%{datacenter}',
        'common'
      ]

      write_hiera_config_on(host, hierarchy)
      scp_to(host, "#{proj_root}/hieradata", "/tmp/hieradata")
      # Bug where windows can't validate the cert when using https
      forge_repo = '--module_repository=http://artifactory.ap.org/api/puppet/puppet'

      module_name = proj_name.split('-').last

      if host['platform'] =~ /windows/ then
        set:backend,:winrm

        username = ENV['TARGET_HOST_USERNAME'] || 'vagrant'
        password = ENV['TARGET_HOST_PASSWORD'] || 'vagrant'
        endpoint = 'http://127.0.0.1:5985/wsman'

        opts = {
          user: username,
          password: password,
          endpoint: endpoint,
          operation_timeout: 300,
        }

        c.winrm = ::WinRM::Connection.new(opts)
        Specinfra.configuration.winrm = c.winrm
      end

      Dir.foreach("#{proj_root}/spec/fixtures/modules/") do |fname|
        next if fname == '.' or fname == '..'
        puppet_module_install_on(host, :source => "#{proj_root}/spec/fixtures/modules/#{fname}", :module_name => fname)
      end

      # This will work fine on *nix systems because when the modules get removed and installed the current module is a symlink
      # However on windows hosts when the symlink gets deleted it deletes the source dir which is where the code is and when it
      # goes to install after it will fail. So we'll remove this step for Windows hosts.
      if host['platform'] !~ /windows/ then
        puppet_module_install_on(host, :source => proj_root, :module_name => module_name)
      end
    end
  end
end
