require 'spec_helper'

describe 'role::example_win' do
  on_supported_os({
    supported_os: [
      {
        'operatingsystem' => 'windows',
        'operatingsystemrelease' => [
          '2012 R2'
        ]
      }
    ]
  }).each do |os, facts|
    context "on #{os}" do
      let(:facts) do
        facts.merge({
          puppetversion: Puppet.version,
          role: 'example_win'
        })
      end
      %w{role::example_win profile::example}.each do |puppet_class|
        it { is_expected.to contain_class(puppet_class) }
      end
      it { is_expected.to compile.with_all_deps }
    end
  end
end
