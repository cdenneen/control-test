require 'spec_helper'
require 'puppet/confine/exists'

describe 'profile::example' do
  on_supported_os({
    supported_os: [
      {
        'operatingsystem' => 'CentOS',
        'operatingsystemrelease' => [
          '6',
          '7'
        ]
      }
    ]
  }).each do |os, facts|
    context "on #{os}" do
      let(:facts) do
        facts.merge({
          puppetversion: Puppet.version
        })
      end
      %w{profile::example}.each do |puppet_class|
        it { is_expected.to contain_class(puppet_class) }
      end
      it { is_expected.to contain_notify('Example Profile') }
      it { is_expected.to compile.with_all_deps }
    end
  end
end
